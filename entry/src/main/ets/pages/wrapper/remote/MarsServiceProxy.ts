/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/

import { Context } from '@ohos.abilityAccessCtrl';
import { GlobalContext } from '@ohos/mars';

class MarsServiceProxy {
  public static inst: MarsServiceProxy;
  private static gPackageName: string;
  private static gClassName: string;

  public static readonly SERVICE_DEFAULT_CLASSNAME: string = "sample.wrapper.service.MarsServiceNative";

  public static init(packageName: string): void {
    if (MarsServiceProxy.inst != null) {
      // TODO: Already initialized
      return;
    }

    let context = GlobalContext.getContext().getValue('context') as Context;
    MarsServiceProxy.gPackageName = (packageName == null ? context.applicationInfo.name : packageName);
    MarsServiceProxy.gClassName = MarsServiceProxy.SERVICE_DEFAULT_CLASSNAME;
    MarsServiceProxy.inst = new MarsServiceProxy();
  }

  public setForeground(isForeground: boolean): void {}
}

export default MarsServiceProxy;