/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { GlobalContext } from '@ohos/mars';
import protobuf from '@ohos/protobufjs'
import { MarsServiceStub } from '../../wrapper/service/MarsServiceStub';
import MessageBuilder from './MessageBuilder'

/**
 * SendMessageRequest proto
 *
 * message SendMessageRequest {
 *    required string access_token = 1;
 *    required string from = 2;
 *    required string to = 3;
 *    required string text = 4;
 *    required string topic = 5;
 * }
 */
export default class SendMessageRequest extends MessageBuilder {
  private access_token: string;
  private from: string;
  private to: string;
  private text: string;
  private topic: string;

  private static proto: string =
    "message SendMessageRequest {" +
      "required string access_token = 1;" +
      "required string from = 2;" +
      "required string to = 3;" +
      "required string text = 4;" +
      "required string topic = 5;" +
      "}";


  public buildMessage() {
    console.log("enter SendMessageRequest.buildMessage")
    console.log("SendMessageRequest.proto:" + SendMessageRequest.proto);
    let Request = GlobalContext.getContext().getValue('request')
    // @ts-ignore
    let msg: protobuf.Builder.Message = new Request(this);
    console.info("create msg ok");

    ///*
    let buffer: ArrayBuffer = msg.toArrayBuffer();
    console.info("buffer:" + new Uint8Array(buffer));
    console.info("buffer1:" + MarsServiceStub.Uint8Array2String(new Uint8Array(buffer)));
    try {
      // @ts-ignore
      let decodeMsg = Request.decode(buffer);
      console.info("decode:" + JSON.stringify(decodeMsg));
    } catch (e) {
      console.info("decode exception:" + e);
    }
    let tmpBuffer = new Uint8Array([0x0a, 0x0a, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x12, 0x05, 0x74, 0x79, 0x69, 0x6a, 0x68, 0x1a, 0x03, 0x61, 0x6c, 0x6c, 0x22, 0x08, 0x37, 0x37, 0x37, 0x38, 0x38, 0x38, 0x38, 0x38, 0x2a, 0x01, 0x30]);
    console.info("buffer:" + tmpBuffer);
    console.info("buffer2:" + MarsServiceStub.Uint8Array2String(tmpBuffer));
    try {
      // @ts-ignore
      let decodeMsg = Request.decode(tmpBuffer);
      console.info("decode:" + JSON.stringify(decodeMsg));
    } catch (e) {
      console.info("decode exception:" + e);
    }

    //*/
    return msg;
  }

  public getAccessToken(): string {
    return this.access_token;
  }

  public setAccessToken(access_token: string): void {
    this.access_token = access_token;
  }

  public getFrom(): string {
    return this.from;
  }

  public setFrom(from: string): void {
    this.from = from;
  }

  public getTo(): string {
    return this.to;
  }

  public setTo(to: string): void {
    this.to = to;
  }

  public getText(): string {
    return this.text;
  }

  public setText(text: string): void {
    this.text = text;
  }

  public getTopic(): string {
    return this.topic;
  }

  public setTopic(topic: string): void {
    this.topic = topic;
  }
}