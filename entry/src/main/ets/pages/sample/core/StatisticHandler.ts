/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/

import BusinessHandler from './BusinessHandler';
import PushMessage from '../wrapper/remote/PushMessage';
import BaseConstants from '../utils/print/BaseConstants';
import { MarsServiceStub } from '../wrapper/service/MarsServiceStub'
import { TaskProfile, SignalDetectResult, GlobalContext } from '@ohos/mars';

class StatisticHandler extends BusinessHandler {
  public static taskHistory: TaskProfile[] = [];
  public static sdtResults: SignalDetectResult[] = [];
  public static flows: string[] = [];
  public static wifiRecvFlow: number = 0;
  public static wifiSendFlow: number = 0;
  public static mobileRecvFlow: number = 0;
  public static mobileSendFlow: number = 0;

  public async handleRecvMessage(pushMessage: PushMessage): Promise<boolean> {
    console.info("enter handleRecvMessage cmdId: " + pushMessage.cmdId);
    let pushMessageString = MarsServiceStub.Uint8Array2String(new Uint8Array(pushMessage.buffer));
    console.info("enter handleRecvMessage pushMessageString: " + pushMessageString);
    try {
      switch (pushMessage.cmdId) {
        case BaseConstants.CGIHISTORY_CMDID: {
          console.info("StatisticHandler handleRecvMessage BaseConstants.CGIHISTORY_CMDID");
          let profile: TaskProfile = JSON.parse(pushMessageString);
          console.info("StatisticHandler handleRecvMessage getFrom:" + profile.toString());
          console.info("StatisticHandler handleRecvMessage cmdId:" + profile.cmdId);
          console.info("StatisticHandler handleRecvMessage cgi:" + profile.cgi);
          console.info("StatisticHandler handleRecvMessage channelSelect:" + profile.channelSelect);
          console.info("StatisticHandler handleRecvMessage dyntimeStatus:" + profile.dyntimeStatus);
          StatisticHandler.taskHistory.push(profile);

          GlobalContext.getContext().setValue('taskHistory', StatisticHandler.taskHistory)
          return true;
        }
        case BaseConstants.CONNSTATUS_CMDID: {
          console.info("StatisticHandler handleRecvMessage BaseConstants.CONNSTATUS_CMDID");
          return true;
        }
        case BaseConstants.FLOW_CMDID: { //209,0
          console.info("StatisticHandler handleRecvMessage BaseConstants.FLOW_CMDID");
          let flowsizes: string[] = pushMessageString.split(",");
          StatisticHandler.wifiSendFlow += Number(flowsizes[0]);
          StatisticHandler.wifiRecvFlow += Number(flowsizes[1]);
          StatisticHandler.mobileSendFlow += Number(flowsizes[2]);
          StatisticHandler.mobileRecvFlow += Number(flowsizes[3]);
          console.info("enter handleRecvMessage BaseConstants.FLOW_CMDID wifiSendFlow=" + Number(flowsizes[0]) +
            ", wifiRecvFlow=" + Number(flowsizes[1]) +
            ", mobileSendFlow=" + Number(flowsizes[2]) +
            ", mobileRecvFlow=" + Number(flowsizes[3]));

          GlobalContext.getContext().setValue('wifiSendFlow', StatisticHandler.wifiSendFlow)
          GlobalContext.getContext().setValue('wifiRecvFlow', StatisticHandler.wifiRecvFlow)
          return true;
        }
        case BaseConstants.SDTRESULT_CMDID: {
          console.info("StatisticHandler handleRecvMessage BaseConstants.SDTRESULT_CMDID");
          let profile: SignalDetectResult = JSON.parse(JSON.stringify(pushMessageString));
          StatisticHandler.sdtResults.push(profile);
          GlobalContext.getContext().setValue('sdtResults', StatisticHandler.sdtResults)
          return true;
        }
        default:
          break;
      }
    } catch (error) {
      console.error('process failed.handleRecvMessage Cause: ' + error);
    }

    return false;
  }
}

export default StatisticHandler;