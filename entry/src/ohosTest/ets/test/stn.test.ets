/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { describe, it, expect } from '@ohos/hypium';
import { StnLogic, TaskProfile } from '@ohos/mars'

export default function stnTest() {
  describe('StnTest', () => {
    class TempStub implements StnLogic.ICallBack {
      makesureAuthed(host: string): boolean {
        return false
      }

      onNewDns(host: string): string[] {
        return []
      }

      onPush(cmdid: number, taskid: number, data: ArrayBuffer) {
      }

      req2Buf(taskID: number, userContext: ESObject, errCode: number[], channelSelect: number, host: string): ArrayBuffer {
        return new ArrayBuffer(0)
      }

      buf2Resp(taskID: number, userContext: ESObject, respBuffer: ArrayBuffer, errCode: number[], channelSelect: number): number {
        return 0;
      }

      trafficData(send: number, recv: number) {
      }

      reportConnectInfo(status: number, longlinkstatus: number) {
      }

      getLongLinkIdentifyCheckBuffer(identifyReqBuf: ArrayBuffer, hashCodeBuffer: ArrayBuffer, reqRespCmdID: number[]): number {
        return 0;
      }

      onLongLinkIdentifyResp(buffer: number[], hashCodeBuffer: number[]): boolean {
        return false
      }

      requestDoSync() {
      }

      requestNetCheckShortLinkHosts(): string[] {
        return []
      }

      isLogoned(): boolean {
        return false
      }

      reportTaskProfile(taskString: string) {
      }

      onTaskEnd(taskID: number, userContext: ESObject, errType: number, errCode: number, profile: StnLogic.CgiProfile): number {
        return 0;
      }
    }

    it('setCallBack', 0, () => {
      expect(StnLogic.setCallBack(new TempStub())).assertUndefined()
    })

    it('setLongLinkSvrAddr', 0, () => {
      expect(StnLogic.setLonglinkSvrAddr('', [])).assertEqual(0)
    })

    it('setShortLinkSvrAddr', 0, () => {
      expect(StnLogic.setShortlinkSvrAddr(0)).assertEqual(0)
    })

    it('setDebugIP', 0, () => {
      expect(StnLogic.setDebugIP('', '')).assertEqual(0)
    })

    it('startTask', 0, () => {
      let shortLinkHostList = new Array("shortLink01", "shortLink02", "shortLink03");
      expect(StnLogic.startTask(new StnLogic.Task(StnLogic.Task.EShort, 0, "cgi", shortLinkHostList))).assertEqual(0)
    })

    it('stopTask', 0, () => {
      expect(StnLogic.stopTask(0)).assertEqual(0)
    })

    it('hasTask', 0, () => {
      expect(StnLogic.hasTask(0)).assertTrue()
    })
    
    it('setBackupIPs', 0, () => {
      expect(StnLogic.setBackupIPs('', [])).assertEqual(0)
    })

    it('makeSureLongLinkConnected', 0, () => {
      expect(StnLogic.makesureLongLinkConnected()).assertEqual(0)
    })

    it('setSignallingStrategy', 0, () => {
      expect(StnLogic.setSignallingStrategy(0, 0)).assertEqual(0)
    })

    it('keepSignalling', 0, () => {
      expect(StnLogic.keepSignalling()).assertEqual(0)
    })

    it('stopSignalling', 0, () => {
      expect(StnLogic.stopSignalling()).assertEqual(0)
    })

    it('setClientVersion', 0, () => {
      expect(StnLogic.setClientVersion(0)).assertEqual(0)
    })

    it('getLoadLibraries', 0, () => {
      expect(typeof StnLogic.getLoadLibraries()).assertEqual('undefined')
    })

    it('genTaskID', 0, () => {
      expect(StnLogic.genTaskID()).assertEqual(2)
    })

    it('req2Buf', 0, () => {
      expect(typeof StnLogic.req2Buf(0, 0, 0, '')).assertEqual('object')
    })

    it('buf2Resp', 0, () => {
      expect(StnLogic.buf2Resp(0, 0, new ArrayBuffer(0), 0)).assertEqual(0)
    })

    it('trafficData', 0, () => {
      expect(StnLogic.trafficData(0, 0)).assertUndefined()
    })

    it('TaskProfile', 0, () => {
      expect(typeof TaskProfile.toString()).assertEqual('string')
    })
  })
}