/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import marsnapi from 'libmarsnapi.so';

/**
 * 信令探测工具类
 */
class SdtLogic {

  public static readonly TAG: string = "mars.SdtLogic";

  static NetCheckType = class {
    static readonly kPingCheck: number = 0;
    static readonly kDnsCheck: number = 1;
    static readonly kNewDnsCheck: number = 2;
    static readonly kTcpCheck: number = 3;
    static readonly kHttpCheck: number = 4;
  };

  static TcpCheckErrCode = class {
    static readonly kTcpSucc: number = 0;
    static readonly kTcpNonErr: number = 1;
    static readonly kSelectErr: number = -1;
    static readonly kPipeIntr: number = -2;
    static readonly kSndRcvErr: number = -3;
    static readonly kAssertErr: number = -4;
    static readonly kTimeoutErr: number = -5;
    static readonly kSelectExpErr: number = -6;
    static readonly kPipeExp: number = -7;
    static readonly kConnectErr: number = -8;
    static readonly kTcpRespErr: number = -9;
  };

  /*static {
    Mars.loadDefaultMarsLibrary();
  }*/

  private static callBack: SdtLogic.ICallBack = null;

  /**
   * 设置信令探测回调实例，探测结果将通过该实例通知上层
   * @param _callBack
   */
  public static setCallBack(_callBack: SdtLogic.ICallBack): void {
    SdtLogic.callBack = _callBack;
  }

  /**
   * 设置一个Http连通状态探测的URI
   * @param requestURI
   */
  public static setHttpNetcheckCGI(requestURI: string): number{
    var result = marsnapi.setHttpNetcheckCGI(requestURI);
    console.log("Mars: setHttpNetcheckCGI result " + result);
    return result
  };

  /*
   * 获取底层已加载模块
   * @return
   */
  private static getLoadLibraries():Array<string> {
    var result = marsnapi.getLoadLibraries();
    console.log("Mars: getLoadLibraries result " + result);
    return result
  };

  private static reportSignalDetectResults(resultsJson: string): void {
    try {
      if (SdtLogic.callBack == null) {
        console.info('callback is null');
        return;
      }
      SdtLogic.callBack.reportSignalDetectResults(resultsJson);
    } catch (error) {
      console.info('Process Failed.reportSignalDetectResults Cause: ' + error);
    }
  }
}

namespace SdtLogic {

  export type NetCheckType = typeof SdtLogic.NetCheckType.prototype;

  export type TcpCheckErrCode = typeof SdtLogic.TcpCheckErrCode.prototype;

  /**
   * 信令探测回调接口，启动信令探测
   */
  export interface ICallBack {
    reportSignalDetectResults(resultsJson: string): void;
  }
}

export default SdtLogic;